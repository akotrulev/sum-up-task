package api.transaction;

import api.AuthenticationAction;
import api.BaseApiTest;
import org.testng.annotations.Test;
import pojo.transaction.history.HistoryResponsePojo;
import utility.CustomAssert;

import java.util.Collections;

public class HistoryTest extends BaseApiTest {

    @Test
    public void getAllTransaction() {
        String token = new AuthenticationAction().getAuthTokenAsValidUser();
        HistoryResponsePojo historyResponsePojo = new HistoryAction(token)
                .getHistoryOfAnUser(Collections.emptyMap());

        CustomAssert customAssert = new CustomAssert();
        customAssert.assertEquals(historyResponsePojo.getItems().size(), 0);
        customAssert.assertAll();
    }
}
