package api.account;

import api.AuthenticationAction;
import api.BaseApiTest;
import org.testng.annotations.Test;
import pojo.account.BankAccountPojo;
import utility.CustomAssert;
import utility.SystemPropertyUtil;

import java.util.List;

public class BankAccountTest extends BaseApiTest {

    @Test
    public void verifyBankAccountData() {
        String token = new AuthenticationAction().getAuthTokenAsValidUser();

        List<BankAccountPojo> bankAccountPojoList = new BankAccountAction(token).getBankAccountPojoList();
        CustomAssert customAssert = new CustomAssert();
        customAssert.assertEquals(bankAccountPojoList.size(), 1);
        customAssert.assertEquals(bankAccountPojoList.get(0), SystemPropertyUtil.getUserDetail().getBankDetails().get(0));
        customAssert.assertAll();
    }
}
