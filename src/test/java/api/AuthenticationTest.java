package api;

import org.testng.annotations.Test;
import pojo.authentication.AuthenticationRequestPojo;
import pojo.authentication.AuthenticationResponsePojo;
import utility.CustomAssert;
import utility.SystemPropertyUtil;

public class AuthenticationTest extends BaseApiTest {
    @Test
    public void loginIsSuccessful() {
        AuthenticationRequestPojo requestPojo = SystemPropertyUtil.getUserDetail().getUserDetails();
        AuthenticationResponsePojo responsePojo = new AuthenticationAction().loginResponsePojo(requestPojo);
        CustomAssert.isTrue(!responsePojo.getAccessToken().isEmpty());
    }
}
