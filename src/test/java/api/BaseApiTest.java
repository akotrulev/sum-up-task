package api;

import io.restassured.RestAssured;
import utility.SystemPropertyUtil;

public class BaseApiTest {

    public BaseApiTest() {
        SystemPropertyUtil.loadAllPropsFromFiles();
        RestAssured.baseURI = SystemPropertyUtil.getBaseApiUrl();
    }
}
