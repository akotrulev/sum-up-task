package pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import pojo.account.BankAccountPojo;
import pojo.authentication.AuthenticationRequestPojo;

import java.util.List;

//This class is only for holding test data
@Data
@NoArgsConstructor
public class UserData {
    private AuthenticationRequestPojo userDetails;
    private List<BankAccountPojo> bankDetails;
}
