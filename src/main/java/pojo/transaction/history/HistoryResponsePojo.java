package pojo.transaction.history;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class HistoryResponsePojo {

    private List<ItemPojo> items;
    private List<LinkPojo> links;
}
