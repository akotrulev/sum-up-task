package pojo.transaction.history;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ItemPojo {
    private String id;
    @JsonProperty("transaction_code")
    private String transactionCode;
    private int amount ;
    private String currency;
    private String timestamp;
    private String status;
    private String payment_type;
    private int installments_count;
    private String product_summary;
    private int payouts_total;
    private int payouts_received;
    private String payout_plan;
    private String transaction_id;
    private String user;
    private String type;
    private String card_type;
}
