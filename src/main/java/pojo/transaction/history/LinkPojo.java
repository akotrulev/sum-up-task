package pojo.transaction.history;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

public class LinkPojo {

    private String rel;
    private String href;
    private String type;
}
