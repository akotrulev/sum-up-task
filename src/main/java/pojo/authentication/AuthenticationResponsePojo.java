package pojo.authentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthenticationResponsePojo {

    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("legacy_token")
    private String legacyToken;
    @JsonProperty("expires_in")
    private int expiresIn;
    @JsonProperty("merchantCode")
    private String merchantCode;
    @JsonProperty("refresh_token")
    private String refreshToken;
    private String scope;
    @JsonProperty("token_type")
    private String tokenType;
}
