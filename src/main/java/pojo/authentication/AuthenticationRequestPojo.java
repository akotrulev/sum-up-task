package pojo.authentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthenticationRequestPojo {

    @JsonProperty(value = "client_id")
    private String clientId;
    @JsonProperty("client_secret")
    private String clientSecret;
    private String username;
    private String password;
}
