package pojo.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor

public class AccountDetailsResponsePojo {

    @JsonProperty("bank_accounts")
    private List<BankAccountPojo> bankAccounts;
}
