package pojo.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class BankAccountPojo {
    @JsonProperty("bank_code")
    private String bankCode;
    @JsonProperty("branch_code")
    private String branchCode;
    private String swift;
    @JsonProperty("account_number")
    private String accountNumber;
    private String iban;
    @JsonProperty("account_type")
    private String accountType;
    @JsonProperty("account_category")
    private String accountCategory;
    @JsonProperty("account_holder_name")
    private String accountHolderName;
    private String status;
    private boolean primary;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("bank_name")
    private String bankName;
}
