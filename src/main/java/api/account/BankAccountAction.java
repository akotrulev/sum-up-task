package api.account;

import api.Request;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.internal.RestAssuredResponseImpl;
import io.restassured.specification.RequestSpecification;
import pojo.account.BankAccountPojo;
import utility.CustomAssert;

import java.util.Arrays;
import java.util.List;

import static org.apache.http.HttpStatus.SC_OK;

public class BankAccountAction {

    private static String PATH = "/v0.1/me/merchant-profile/bank-accounts";
    private String token;

    public BankAccountAction(String token) {
        this.token = token;
    }

    public RestAssuredResponseImpl getBankAccountResponse() {
        RequestSpecification requestSpecification = new RequestSpecBuilder()
                .setBasePath(PATH)
                .setAccept(ContentType.JSON)
                .addHeader("Authorization", token)
                .build();
        return new Request().get(requestSpecification);
    }

    public List<BankAccountPojo> getBankAccountPojoList() {
        RestAssuredResponseImpl response = getBankAccountResponse();
        CustomAssert.areEqual(response.getStatusCode(), SC_OK);
        return Arrays.asList(response.as(BankAccountPojo[].class));
    }
}
