package api.account;

import api.Request;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.internal.RestAssuredResponseImpl;
import io.restassured.specification.RequestSpecification;
import pojo.account.AccountDetailsResponsePojo;
import utility.CustomAssert;

import static org.apache.http.HttpStatus.SC_OK;

public class AccountAction {

    private static String PATH = "/v0.1/me";
    private String token;

    public AccountAction(String token) {
        this.token = token;
    }

    public RestAssuredResponseImpl getAccountResponse() {
        RequestSpecification requestSpecification = new RequestSpecBuilder()
                .setBasePath(PATH)
                .setAccept(ContentType.JSON)
                .addHeader("Authorization", token)
                .build();
        return new Request().get(requestSpecification);
    }

    public AccountDetailsResponsePojo getAccountPojo() {
        RestAssuredResponseImpl response = getAccountResponse();
        CustomAssert.areEqual(response.getStatusCode(), SC_OK);
        return response.as(AccountDetailsResponsePojo.class);
    }
}
