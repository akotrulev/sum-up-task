package api;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.internal.RestAssuredResponseImpl;
import io.restassured.specification.RequestSpecification;
import pojo.authentication.AuthenticationRequestPojo;
import pojo.authentication.AuthenticationResponsePojo;
import utility.CustomAssert;
import utility.SystemPropertyUtil;

import static org.apache.http.HttpStatus.SC_OK;

public class AuthenticationAction {
    private static String PATH = "/oauth";

    public RestAssuredResponseImpl login(AuthenticationRequestPojo requestPojo) {
        RequestSpecification requestSpecification = new RequestSpecBuilder()
                .setBody(requestPojo)
                .setBasePath(PATH)
                .setContentType(ContentType.JSON)
                .build();
        return new Request().post(requestSpecification);
    }

    public AuthenticationResponsePojo loginResponsePojo(AuthenticationRequestPojo requestPojo) {
        RestAssuredResponseImpl response = login(requestPojo);
        CustomAssert.areEqual(response.getStatusCode(), SC_OK);
        return response.as(AuthenticationResponsePojo.class);
    }

    public String getAuthToken(AuthenticationRequestPojo requestPojo) {
        AuthenticationResponsePojo responsePojo = loginResponsePojo(requestPojo);
        return String.format("%s %s",responsePojo.getTokenType(), responsePojo.getAccessToken());
    }

    public String getAuthTokenAsValidUser() {
        return getAuthToken(SystemPropertyUtil.getUserDetail().getUserDetails());
    }
}
