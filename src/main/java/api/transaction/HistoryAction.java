package api.transaction;

import api.Request;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.internal.RestAssuredResponseImpl;
import io.restassured.specification.RequestSpecification;
import pojo.transaction.history.HistoryResponsePojo;
import utility.CustomAssert;

import java.util.Map;

import static org.apache.http.HttpStatus.SC_OK;

public class HistoryAction {
    private static String PATH = "/v0.1/me/transactions/history";
    private String token;

    public HistoryAction(String authToken) {
        token = authToken;
    }
    public RestAssuredResponseImpl getHistory(Map<String, String> queryParams) {
        RequestSpecification requestSpecification = new RequestSpecBuilder()
                .addParams(queryParams)
                .setBasePath(PATH)
                .addHeader("Authorization", token)
                .setAccept(ContentType.JSON)
                .build();

        return new Request().get(requestSpecification);
    }

    public HistoryResponsePojo getHistoryOfAnUser(Map<String, String> queryParams) {
        RestAssuredResponseImpl response = getHistory(queryParams);
        CustomAssert.areEqual(response.getStatusCode(), SC_OK);
        return response.as(HistoryResponsePojo.class);
    }
}
