package utility;

import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import org.testng.IAlterSuiteListener;
import org.testng.xml.XmlSuite;
import java.util.List;

public class Listener extends ExtentITestListenerClassAdapter implements IAlterSuiteListener {
    @Override
    public void alter(List<XmlSuite> suites) {
        XmlSuite suite = suites.get(0);
        List<String> include = SystemPropertyUtil.getIncludeGroups();
        List<String> exclude = SystemPropertyUtil.getExcludeGroups();
        if (!include.isEmpty()) {
            include.forEach(suite::addIncludedGroup);
        }
        if (!exclude.isEmpty()) {
            exclude.forEach(suite::addExcludedGroup);
        }
    }
}
